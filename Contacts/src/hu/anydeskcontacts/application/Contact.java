package hu.anydeskcontacts.application;

import java.io.Serializable;

public class Contact implements Serializable {

    private int id;
    private String name;
    private String anydeskId;
    private Location location;
    private Type type;

    public Contact(int id, String name, String anydeskId, Location location, Type type) {
        this.id = id;
        this.name = name;
        this.anydeskId = anydeskId;
        this.location = location;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnydeskId() {
        return anydeskId;
    }

    public void setAnydeskId(String anydeskId) {
        this.anydeskId = anydeskId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public enum Location {

        SZABADM, JÓKAI, KADA, JÓZSEFA, KECSKEMÉT, CENTI, ANKER, ÚJPEST, LEGÉNY, BARTÓK, BUDAÖRSI, DAMJANICH, THÖKÖLY, SUGÁR;
    }

    public enum Type {

        TOBACCOSHOP, SHOP;
    }
}
