package hu.anydeskcontacts.application;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MainFrame3 extends javax.swing.JFrame implements ListSelectionListener, ActionListener, ChangeListener, MouseListener {

    private javax.swing.JButton btnAddNew;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JCheckBox chEdit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel pnEdit;
    private javax.swing.JList list;

    private javax.swing.JScrollPane pnScroll;
    private javax.swing.JTextField txtAlias;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtNewAlias;
    private javax.swing.JTextField txtNewName;
    // End of variables declaration
    private DefaultListModel listModel = new DefaultListModel();

    ConnectionManager cm;

    public MainFrame3(ConnectionManager cm) {
        this.cm = cm;
        setLocationRelativeTo(this);
        initComponents();
        for (int i = 0; i < cm.contacts.size(); i++) {
            String temptext = extra.Format.right(cm.contacts.get(i).getId(), 2) + " " + cm.contacts.get(i).getName();
            System.out.println(temptext);
            getListModel().insertElementAt(temptext, i);
            //getListModel().addElement(extra.Format.right(temptext, 12));

        }

        list.setModel(listModel);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        pnScroll = new javax.swing.JScrollPane();
        list = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField(5);
        jLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField(15);
        jLabel3 = new javax.swing.JLabel();
        txtAlias = new javax.swing.JTextField(15);
        chEdit = new javax.swing.JCheckBox();
        pnEdit = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtNewName = new javax.swing.JTextField(15);
        jLabel5 = new javax.swing.JLabel();
        txtNewAlias = new javax.swing.JTextField(15);
        btnAddNew = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.addListSelectionListener(this);
        list.addMouseListener(this);
        pnScroll.setViewportView(list);
        jLabel1.setText("Id:");
        txtId.setText("");
        jLabel2.setText("Név:");
        txtName.setText("");
        jLabel3.setText("AnyDesk cím:");
        txtAlias.setText("");
        chEdit.setText("Szerkesztés");
        chEdit.addChangeListener(this);
        pnEdit.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pnEdit.setVisible(false);
        jLabel4.setText("Név:");
        txtNewName.setText("");
        jLabel5.setText("Cím:");
        txtNewAlias.setText("");
        btnAddNew.setText("Hozzáadás");
        btnAddNew.addActionListener(this);
        btnUpdate.setText("Frissít");
        txtName.setEditable(false);
        txtAlias.setEditable(false);
        txtId.setEditable(false);
        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(pnEdit);
        pnEdit.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addComponent(jLabel4)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(txtNewName, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addComponent(jLabel5)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(txtNewAlias)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(btnAddNew))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(btnUpdate)))
                                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(btnUpdate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel4)
                                        .addComponent(txtNewName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel5)
                                        .addComponent(txtNewAlias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnAddNew))
                                .addGap(20, 20, 20))
        );

        btnSave.setText("Mentés");
        btnSave.addActionListener(this);
        btnUpdate.addActionListener(this);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(pnScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnSave))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel1)
                                                .addGap(18, 18, 18)
                                                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(54, 54, 54)
                                                .addComponent(jLabel2)
                                                .addGap(32, 32, 32)
                                                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel3)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txtAlias))
                                        .addComponent(chEdit)
                                        .addComponent(pnEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(pnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                        .addComponent(jLabel1)
                                                                        .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jLabel2)
                                                                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                        .addComponent(jLabel3)
                                                                        .addComponent(txtAlias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(51, 51, 51)
                                                                .addComponent(chEdit))
                                                        .addComponent(pnScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(btnSave)
                                                .addContainerGap(20, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>

    // Variables declaration - do not modify
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (list.getSelectedIndex() >= 0) {
            cm.listChange(list.getSelectedIndex());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnAddNew) {
            cm.addNewContact();
        } else if (e.getSource() == btnSave) {
            cm.saveTheList();
        } else if (e.getSource() == btnUpdate) {
            cm.modifyList();
        }

    }

    public void addToList(String newElement) {
        System.out.println("egyik: " + txtNewAlias.getText());
        System.out.println("Másik" + txtNewName.getText());
        if (txtNewAlias.getText().equals("") || txtNewName.getText().equals("")) {
            JOptionPane.showMessageDialog(null,
                    "Nincs kitöltve minden mező!",
                    "Figyelem!",
                    JOptionPane.INFORMATION_MESSAGE);
        } else {
            System.out.println("Ide írunk: " + cm.contacts.size());
            listModel.insertElementAt(" " + (cm.contacts.size()) + " " + getTxtNewName().getText(), cm.contacts.size() - 1);
            txtNewAlias.setText("");
            txtNewName.setText("");
        }

    }

    public void changeTheList() {
        int selectedIndex = list.getSelectedIndex();
        listModel.setElementAt(" " + (selectedIndex + 1) + " " + txtName.getText(), selectedIndex);

    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (chEdit.isSelected()) {
            pnEdit.setVisible(true);
            txtName.setEditable(true);
            txtAlias.setEditable(true);
        } else {
            pnEdit.setVisible(false);
            txtName.setEditable(false);
            txtAlias.setEditable(false);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == list) {
            if (e.getClickCount() == 2) {

                // Double-click detected
                cm.startConnection(list.getSelectedIndex());
            }
        }
    }

    public DefaultListModel getListModel() {
        return listModel;
    }

    public JTextField getTxtId() {
        return txtId;
    }

    public void setTxtId(JTextField txtId) {
        this.txtId = txtId;
    }

    public JTextField getTxtName() {
        return txtName;
    }

    public JTextField getTxtNewAlias() {
        return txtNewAlias;
    }

    public JTextField getTxtNewName() {
        return txtNewName;
    }

    public void setTxtName(JTextField txtName) {
        this.txtName = txtName;
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public JList getList() {
        return list;
    }

    public JTextField getTxtAlias() {
        return txtAlias;
    }

    public void setTxtAlias(JTextField txtAlias) {
        this.txtAlias = txtAlias;
    }
}
