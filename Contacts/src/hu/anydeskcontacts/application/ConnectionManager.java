package hu.anydeskcontacts.application;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class ConnectionManager {

    List<Contact> contacts = new ArrayList();

    MainFrame3 mf;

    public ConnectionManager() {
        //applicationInicialize();
        loadTheList();
        mf = new MainFrame3(this);
        mf.setVisible(true);
        mf.getList().setSelectedIndex(0);

    }

    private void applicationInicialize() {
        contacts.add(new Contact(1, "Anker", "1212123123", Contact.Location.ANKER, Contact.Type.SHOP));
        contacts.add(new Contact(2, "Centi", "121223423", Contact.Location.CENTI, Contact.Type.SHOP));
        contacts.add(new Contact(3, "Kada", "121242344", Contact.Location.KADA, Contact.Type.SHOP));
        contacts.add(new Contact(4, "Anker", "1212123123", Contact.Location.ANKER, Contact.Type.SHOP));
        contacts.add(new Contact(5, "Centi", "121223423", Contact.Location.CENTI, Contact.Type.SHOP));
        contacts.add(new Contact(6, "Kada", "121242344", Contact.Location.KADA, Contact.Type.SHOP));
    }

    public void listChange(int index) {
        mf.getTxtId().setText(String.valueOf(contacts.get(index).getId()));
        mf.getTxtAlias().setText(contacts.get(index).getAnydeskId());
        mf.getTxtName().setText(contacts.get(index).getName());
    }

    public void saveTheList() {
        try {
            FileOutputStream fos = new FileOutputStream("packagedatabase");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(contacts);
            oos.close();
            fos.close();
            JOptionPane.showMessageDialog(null,
                    "Mentés megtörtént!",
                    "Figyelem!",
                    JOptionPane.INFORMATION_MESSAGE);

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void loadTheList() {
        try {
            FileInputStream fis = new FileInputStream("packagedatabase");
            ObjectInputStream ois = new ObjectInputStream(fis);
            contacts = (ArrayList) ois.readObject();
            ois.close();
            fis.close();
//            JOptionPane.showMessageDialog(null,
//            "Betöltés megtörtént!",
//                    "Figyelem!",
//                    JOptionPane.INFORMATION_MESSAGE
//         );
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
    }

    public void addNewContact() {
        int contactsSize = contacts.size();
        contacts.add(new Contact((contactsSize + 1), mf.getTxtNewName().getText(), mf.getTxtNewAlias().getText(), Contact.Location.JÓKAI, Contact.Type.SHOP));
        mf.addToList(mf.getTxtNewName().getText());
    }

    public void modifyList() {
        int selectedIndex = mf.getList().getSelectedIndex();
        contacts.get(selectedIndex).setName(mf.getTxtName().getText());
        contacts.get(selectedIndex).setAnydeskId(mf.getTxtAlias().getText());
        mf.changeTheList();

    }

    public void startConnection(int index) {
        System.out.println("első: " + "C:\\Program Files (x86)\\AnyDesk\\AnyDesk.exe");
        System.out.println("második: " + mf.getTxtAlias().getText());
        try {
            Process futas = new ProcessBuilder("C:\\Program Files (x86)\\AnyDesk\\AnyDesk.exe", contacts.get(index).getAnydeskId()).start();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }

}
