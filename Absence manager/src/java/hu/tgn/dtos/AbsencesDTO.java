package hu.tgn.dtos;

import java.io.Serializable;
import java.util.Date;

public class AbsencesDTO implements Serializable {

    private int absenceId;
    private Date absenceDay;
    private String absenceStatus;
    private String absenceType;
    private int absenceRequestId;
    private int employeeId;

    public AbsencesDTO() {
    }

    public AbsencesDTO(int absenceId) {
        this.absenceId = absenceId;
    }

    public AbsencesDTO(Date absenceDay, String absenceStatus, String absenceType, int absenceRequestId, int employeeId) {
        this.absenceDay = absenceDay;
        this.absenceStatus = absenceStatus;
        this.absenceType = absenceType;
        this.absenceRequestId = absenceRequestId;
        this.employeeId = employeeId;
    }

    public AbsencesDTO(int absenceId, Date absenceDay, String absenceStatus, String absenceType, int absenceRequestId, int employeeId) {
        this.absenceId = absenceId;
        this.absenceDay = absenceDay;
        this.absenceStatus = absenceStatus;
        this.absenceType = absenceType;
        this.absenceRequestId = absenceRequestId;
        this.employeeId = employeeId;
    }

    public int getAbsenceRequestId() {
        return absenceRequestId;
    }

    public void setAbsenceRequestId(int absenceRequestId) {
        this.absenceRequestId = absenceRequestId;
    }

    public int getAbsenceId() {
        return absenceId;
    }

    public void setAbsenceId(int absenceId) {
        this.absenceId = absenceId;
    }

    public Date getAbsenceDay() {
        return absenceDay;
    }

    public void setAbsenceDay(Date absenceDay) {
        this.absenceDay = absenceDay;
    }

    public String getAbsenceStatus() {
        return absenceStatus;
    }

    public void setAbsenceStatus(String absenceStatus) {
        this.absenceStatus = absenceStatus;
    }

    public String getAbsenceType() {
        return absenceType;
    }

    public void setAbsenceType(String absenceType) {
        this.absenceType = absenceType;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

}
