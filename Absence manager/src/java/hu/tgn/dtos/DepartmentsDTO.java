package hu.tgn.dtos;

import java.io.Serializable;

public class DepartmentsDTO implements Serializable {

    private int departmentId;
    private String departmentName;
    private int departmentBossId;

    public DepartmentsDTO() {
    }

    public DepartmentsDTO(int departmentId) {
        this.departmentId = departmentId;
    }

    public DepartmentsDTO(String departmentName) {
        this.departmentName = departmentName;
    }

    public DepartmentsDTO(String departmentName, int departmentBossId) {
        this.departmentName = departmentName;
        this.departmentBossId = departmentBossId;
    }

    public DepartmentsDTO(int departmentId, String departmentName, int departmentBossId) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.departmentBossId = departmentBossId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getDepartmentBossId() {
        return departmentBossId;
    }

    public void setDepartmentBossId(int departmentBossId) {
        this.departmentBossId = departmentBossId;
    }

}
