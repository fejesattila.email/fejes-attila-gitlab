package hu.tgn.dtos;

import java.io.Serializable;
import java.util.Date;

public class EmployeesDTO implements Serializable {

    private int employeeId;
    private String firstname;
    private String lastname;
    private Date birthday;
    private int handicapped;
    private int contractstyle;
    private Date startofwork;
    private Date endofwork;
    private int remaininglastyearvacations;
    private String email;
    private int departmentID;
    private int positionID;

    public EmployeesDTO() {
    }

    public EmployeesDTO(int employeeId) {
        this.employeeId = employeeId;
    }

    public EmployeesDTO(int employeeId, String firstname, String lastname, Date birthday, int handicapped, int contractstyle, Date startofwork, Date endofwork, String email, int positionID) {
        this.employeeId = employeeId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.handicapped = handicapped;
        this.contractstyle = contractstyle;
        this.startofwork = startofwork;
        this.endofwork = endofwork;
        this.email = email;
        this.positionID = positionID;
    }

    public EmployeesDTO(String firstname, String lastname, Date birthday, int handicapped, int contractstyle, Date startofwork, Date endofwork, String email, int positionID) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.handicapped = handicapped;
        this.contractstyle = contractstyle;
        this.startofwork = startofwork;
        this.endofwork = endofwork;
        this.email = email;
        this.positionID = positionID;
    }

    public EmployeesDTO(int employeeId, String firstname, String lastname) {
        this.employeeId = employeeId;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getHandicapped() {
        return handicapped;
    }

    public void setHandicapped(int handicapped) {
        this.handicapped = handicapped;
    }

    public int getContractstyle() {
        return contractstyle;
    }

    public void setContractstyle(int contractstyle) {
        this.contractstyle = contractstyle;
    }

    public Date getStartofwork() {
        return startofwork;
    }

    public void setStartofwork(Date startofwork) {
        this.startofwork = startofwork;
    }

    public Date getEndofwork() {
        return endofwork;
    }

    public void setEndofwork(Date endofwork) {
        this.endofwork = endofwork;
    }

    public int getRemaininglastyearvacations() {
        return remaininglastyearvacations;
    }

    public void setRemaininglastyearvacations(int remaininglastyearvacations) {
        this.remaininglastyearvacations = remaininglastyearvacations;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(int departmentID) {
        this.departmentID = departmentID;
    }

    public int getPositionID() {
        return positionID;
    }

    public void setPositionID(int positionID) {
        this.positionID = positionID;
    }

}
