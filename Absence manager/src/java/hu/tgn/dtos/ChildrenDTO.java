package hu.tgn.dtos;

import java.io.Serializable;
import java.util.Date;

public class ChildrenDTO implements Serializable {

    private int childId;
    private Date birthday;
    private int handicapped;
    private int parentId;
    private int parent2Id;

    public ChildrenDTO() {
    }

    public ChildrenDTO(int childId) {
        this.childId = childId;
    }

    public ChildrenDTO(Date birthday, int handicapped, int parentId, int parent2Id) {
        this.birthday = birthday;
        this.handicapped = handicapped;
        this.parentId = parentId;
        this.parent2Id = parent2Id;
    }

    public ChildrenDTO(Date birthday, int handicapped, int parentId) {
        this.birthday = birthday;
        this.handicapped = handicapped;
        this.parentId = parentId;
    }

    public ChildrenDTO(int childId, Date birthday, int handicapped, int parentId) {
        this.childId = childId;
        this.birthday = birthday;
        this.handicapped = handicapped;
        this.parentId = parentId;
    }

    public ChildrenDTO(int childId, Date birthday, int handicapped, int parentId, int parent2Id) {
        this.childId = childId;
        this.birthday = birthday;
        this.handicapped = handicapped;
        this.parentId = parentId;
        this.parent2Id = parent2Id;
    }

    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getHandicapped() {
        return handicapped;
    }

    public void setHandicapped(int handicapped) {
        this.handicapped = handicapped;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getParent2Id() {
        return parent2Id;
    }

    public void setParent2Id(int parent2Id) {
        this.parent2Id = parent2Id;
    }

}
