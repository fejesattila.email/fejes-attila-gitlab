package hu.tgn.dtos;

import hu.tgn.enums.Role;

public class UserDTO {
    
    private Integer id;
    private String userName;
    private String password;
    private String sessionId;
    private Role role;
    private Integer status;
    private Integer firstLogin;
    private Integer passwordChanged;
    private Integer loginAttempts;
    private Integer loggedIn;

    public UserDTO() {
    }

    public UserDTO(Integer id, String userName, String password, String sessionId, Role role, Integer status, Integer firstLogin, Integer passwordChanged, Integer loginAttempts, Integer loggedIn) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.sessionId = sessionId;
        this.role = role;
        this.status = status;
        this.firstLogin = firstLogin;
        this.passwordChanged = passwordChanged;
        this.loginAttempts = loginAttempts;
        this.loggedIn = loggedIn;
    }

    public UserDTO(String userName, String password, String sessionId, Role role, Integer status, Integer firstLogin, Integer passwordChanged, Integer loginAttempts, Integer loggedIn) {
        this.userName = userName;
        this.password = password;
        this.sessionId = sessionId;
        this.role = role;
        this.status = status;
        this.firstLogin = firstLogin;
        this.passwordChanged = passwordChanged;
        this.loginAttempts = loginAttempts;
        this.loggedIn = loggedIn;
    }

    public UserDTO(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Integer firstLogin) {
        this.firstLogin = firstLogin;
    }

    public Integer getPasswordChanged() {
        return passwordChanged;
    }

    public void setPasswordChanged(Integer passwordChanged) {
        this.passwordChanged = passwordChanged;
    }

    public Integer getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(Integer loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    public Integer getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Integer loggedIn) {
        this.loggedIn = loggedIn;
    }
    
    
    
}
