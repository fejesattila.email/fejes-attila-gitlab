package hu.tgn.dtos;

import java.io.Serializable;
import java.util.Date;

public class AbsenceRequestDTO implements Serializable {

    private int id;

    private Date startDate;

    private Date endDate;

    private String type;

    private String status;

    private String requestType;

    private int employeeId;

    public AbsenceRequestDTO() {
    }

    public AbsenceRequestDTO(int id) {
        this.id = id;
    }

    public AbsenceRequestDTO(int id, Date startDate, Date endDate, String type, String status, String requestType, int employeeId) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.status = status;
        this.requestType = requestType;
        this.employeeId = employeeId;
    }

    public AbsenceRequestDTO(Date startDate, Date endDate, String type, String status, String requestType, int employeeId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.status = status;
        this.requestType = requestType;
        this.employeeId = employeeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

}
