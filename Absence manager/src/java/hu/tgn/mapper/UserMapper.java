package hu.tgn.mapper;

import hu.tgn.dtos.UserDTO;
import hu.tgn.entitys.UserEntity;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class UserMapper implements MapperService<UserEntity, UserDTO>{

    @Override
    public UserEntity DTO2Entity(UserDTO userDTO) {
        return new UserEntity(
                userDTO.getId(),
                userDTO.getUserName(), 
                userDTO.getPassword(),
                userDTO.getSessionId(), 
                userDTO.getRole(), 
                userDTO.getStatus(), 
                userDTO.getFirstLogin(),
                userDTO.getPasswordChanged(), 
                userDTO.getLoginAttempts(), 
                userDTO.getLoggedIn()
        );
    }

    @Override
    public UserDTO Entity2DTO(UserEntity entity) {
        return new UserDTO(
                entity.getId(),
                entity.getUserName(), 
                entity.getPassword(),
                entity.getSessionId(), 
                entity.getRole(), 
                entity.getStatus(), 
                entity.getFirstLogin(),
                entity.getPasswordChanged(), 
                entity.getLoginAttempts(), 
                entity.getLoggedIn()
        );
    }
    
    
    
}
