package hu.tgn.mapper;

import hu.tgn.dtos.AbsenceRequestDTO;
import hu.tgn.entitys.AbsenceRequest;
import hu.tgn.entitys.Employees;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class AbsenceRequestMapper implements MapperService<AbsenceRequest, AbsenceRequestDTO> {

    @Override
    public AbsenceRequest DTO2Entity(AbsenceRequestDTO ardto) {
        return new AbsenceRequest(
                ardto.getStartDate(),
                ardto.getEndDate(),
                ardto.getType(),
                ardto.getStatus(),
                ardto.getRequestType(),
                new Employees(ardto.getEmployeeId()));
    }

    @Override
    public AbsenceRequestDTO Entity2DTO(AbsenceRequest entity) {
        return new AbsenceRequestDTO(
                entity.getId(),
                entity.getStartDate(),
                entity.getEndDate(),
                entity.getType(),
                entity.getStatus(),
                entity.getRequestType(),
                entity.getEmployeeId().getEmployeeId());
    }

    public List<AbsenceRequestDTO> entityList2DTOList(List<AbsenceRequest> ars) {
        List<AbsenceRequestDTO> ardtos = new ArrayList<>();
        for (AbsenceRequest request : ars) {
            ardtos.add(Entity2DTO(request));
        }
        return ardtos;
    }

}
