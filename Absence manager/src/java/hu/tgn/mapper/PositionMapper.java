package hu.tgn.mapper;

import hu.tgn.dtos.PositionsDTO;
import hu.tgn.entitys.Positions;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class PositionMapper implements MapperService<Positions, PositionsDTO> {

    @Override
    public Positions DTO2Entity(PositionsDTO pdto) {
        return new Positions(pdto.getPositionName(), pdto.getDangerous(), pdto.getRole());
    }

    @Override
    public PositionsDTO Entity2DTO(Positions position) {
        return new PositionsDTO(position.getPositionId(), position.getPositionName(), position.getDangerous(), position.getRole());
    }

}
