package hu.tgn.mapper;

import hu.tgn.dtos.DepartmentsDTO;
import hu.tgn.entitys.Departments;
import hu.tgn.entitys.Employees;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class DepartmentMapper implements MapperService<Departments, DepartmentsDTO> {

    @Override
    public Departments DTO2Entity(DepartmentsDTO ddto) {
        return new Departments(ddto.getDepartmentName(), new Employees(ddto.getDepartmentBossId()));
    }

    @Override
    public DepartmentsDTO Entity2DTO(Departments departments) {
        return new DepartmentsDTO(departments.getDepartmentId(), departments.getDepartmentName(), departments.getDepartmentBossid().getEmployeeId());
    }

}
