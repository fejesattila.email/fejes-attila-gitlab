package hu.tgn.entitys;

import hu.tgn.enums.Role;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "USER_TABLE")
@NamedQueries({
    @NamedQuery(name = "User.getAll", query = "SELECT u FROM UserEntity u"),
    @NamedQuery(name = "User.findByUsernameAndPassword", query = "SELECT u FROM UserEntity u WHERE u.userName = :username AND u.password = :password"),
    @NamedQuery(name = "User.findById", query = "SELECT u FROM UserEntity u WHERE u.id = :id"),
    @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM UserEntity u WHERE u.userName = :username")
})

public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "USERNAME", nullable = false)
    private String userName;
    @Column(name = "PASSWORD", nullable = false)
    private String password;
    @Column(name = "SESSION_ID", nullable = true)
    private String sessionId;
    @Column(name = "ROLE", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;
    @Column(name = "STATUS", nullable = false, scale = 1) // 0 = passive, 1 = active
    private Integer status;
    @Column(name = "FIRST_LOGIN", scale = 1) // 1 is default, after first login and changing pwd, it becomes 0
    private Integer firstLogin;
    @Column(name = "PASSWORD_CHANGED", scale = 1) // 0 is default, after changing pwd it becomes 1
    private Integer passwordChanged;
    @Column(name = "LOGIN_ATTEMPTS", scale = 1) // 0 is default, it increases if login has faild; after succesful login attempt, it resets to 0
    private Integer loginAttempts;
    @Column(name = "LOGGED_IN", scale = 1) // 0 is default, after succesful login it becomes 1 until logout
    private Integer loggedIn;

    public UserEntity() {
    }

    public UserEntity(String userName, String password, String sessionId, Role role, Integer status, Integer firstLogin, Integer passwordChanged, Integer loginAttempts, Integer loggedIn) {
        this.userName = userName;
        this.password = password;
        this.sessionId = sessionId;
        this.role = role;
        this.status = status;
        this.firstLogin = firstLogin;
        this.passwordChanged = passwordChanged;
        this.loginAttempts = loginAttempts;
        this.loggedIn = loggedIn;
    }
    
    public UserEntity(Integer id, String userName, String password, String sessionId, Role role, Integer status, Integer firstLogin, Integer passwordChanged, Integer loginAttempts, Integer loggedIn) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.sessionId = sessionId;
        this.role = role;
        this.status = status;
        this.firstLogin = firstLogin;
        this.passwordChanged = passwordChanged;
        this.loginAttempts = loginAttempts;
        this.loggedIn = loggedIn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Integer firstLogin) {
        this.firstLogin = firstLogin;
    }

    public Integer getPasswordChanged() {
        return passwordChanged;
    }

    public void setPasswordChanged(Integer passwordChanged) {
        this.passwordChanged = passwordChanged;
    }

    public Integer getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(Integer loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    public Integer getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Integer loggedIn) {
        this.loggedIn = loggedIn;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserEntity)) {
            return false;
        }
        UserEntity other = (UserEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "hu.tgn.login.entity.UserEntity[ id=" + id + " ]";
    }
    
}
