package hu.tgn.entitys;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ABSENCES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Absences.findAll", query = "SELECT a FROM Absences a")
    , @NamedQuery(name = "Absences.findByAbsenceId", query = "SELECT a FROM Absences a WHERE a.absenceId = :absenceId")
    , @NamedQuery(name = "Absences.findByAbsenceDay", query = "SELECT a FROM Absences a WHERE a.absenceDay = :absenceDay")
    , @NamedQuery(name = "Absences.findByAbsenceStatus", query = "SELECT a FROM Absences a WHERE a.absenceStatus = :absenceStatus")
    , @NamedQuery(name = "Absences.findByAbsenceType", query = "SELECT a FROM Absences a WHERE a.absenceType = :absenceType")})
public class Absences implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "s_absence", sequenceName = "s_absence", allocationSize = 1)
    @GeneratedValue(generator = "s_absence", strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
    @Column(name = "ABSENCE_ID")
    private int absenceId;
    @Basic(optional = false)
    @Column(name = "ABSENCE_DAY")
    @Temporal(TemporalType.DATE)
    private Date absenceDay;
    @Basic(optional = false)
    @Column(name = "ABSENCE_STATUS")
    private String absenceStatus;
    @Basic(optional = false)
    @Column(name = "ABSENCE_TYPE")
    private String absenceType;

    @JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "EMPLOYEE_ID")
    @ManyToOne(optional = false)
    private Employees employeeId;

//    @Basic(optional = false)
//    @Column(name = "REQUEST_ID")
//    private BigInteger requestId;
//    @JoinColumn(name = "ABSENCE_ID", referencedColumnName = "ID")
    @OneToOne(optional = false)
    private AbsenceRequest absenceRequest;

    public Absences() {
    }

    public Absences(int absenceId) {
        this.absenceId = absenceId;
    }

    public Absences(int absenceId, Date absenceDay, String absenceStatus, String absenceType, Employees employeeId, AbsenceRequest absenceRequest) {
        this.absenceId = absenceId;
        this.absenceDay = absenceDay;
        this.absenceStatus = absenceStatus;
        this.absenceType = absenceType;
        this.employeeId = employeeId;
        this.absenceRequest = absenceRequest;
    }

    public Absences(Date absenceDay, String absenceStatus, String absenceType, Employees employeeId, AbsenceRequest absenceRequest) {
        this.absenceDay = absenceDay;
        this.absenceStatus = absenceStatus;
        this.absenceType = absenceType;
        this.employeeId = employeeId;
        this.absenceRequest = absenceRequest;
    }

    public int getAbsenceId() {
        return absenceId;
    }

    public void setAbsenceId(int absenceId) {
        this.absenceId = absenceId;
    }

    public Date getAbsenceDay() {
        return absenceDay;
    }

    public void setAbsenceDay(Date absenceDay) {
        this.absenceDay = absenceDay;
    }

    public String getAbsenceStatus() {
        return absenceStatus;
    }

    public void setAbsenceStatus(String absenceStatus) {
        this.absenceStatus = absenceStatus;
    }

    public String getAbsenceType() {
        return absenceType;
    }

    public void setAbsenceType(String absenceType) {
        this.absenceType = absenceType;
    }

    public Employees getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Employees employeeId) {
        this.employeeId = employeeId;
    }

    public AbsenceRequest getAbsenceRequest() {
        return absenceRequest;
    }

    public void setAbsenceRequest(AbsenceRequest absenceRequest) {
        this.absenceRequest = absenceRequest;
    }

    @Override
    public String toString() {
        return "Absences{" + "absenceId=" + absenceId + ", absenceDay=" + absenceDay + ", absenceStatus=" + absenceStatus + ", absenceType=" + absenceType + ", employeeId=" + employeeId + ", absenceRequest=" + absenceRequest + '}';
    }

}
