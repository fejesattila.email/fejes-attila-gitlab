/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.tgn.entitys;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fenyo
 */
@Entity
@Table(name = "DEPARTMENTS")

@NamedQueries({
    @NamedQuery(name = "Departments.findAll", query = "SELECT d FROM Departments d")
    , @NamedQuery(name = "Departments.findByDepartmentId", query = "SELECT d FROM Departments d WHERE d.departmentId = :departmentId")
    , @NamedQuery(name = "Departments.findByDepartmentName", query = "SELECT d FROM Departments d WHERE d.departmentName = :departmentName")})

public class Departments implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "s_department", allocationSize = 1)
    @GeneratedValue(generator = "s_department", strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
    @Column(name = "DEPARTMENT_ID")
    private int departmentId;
    @Basic(optional = false)
    @Column(name = "DEPARTMENT_NAME")
    private String departmentName;
    @OneToMany(mappedBy = "departmentId")
    private Collection<Employees> employeesCollection;
    @JoinColumn(name = "DEPARTMENT_BOSSID", referencedColumnName = "EMPLOYEE_ID")
    @ManyToOne(optional = false)
    private Employees departmentBossid;

    public Departments() {
    }

    public Departments(int departmentId) {
        this.departmentId = departmentId;
    }

    public Departments(int departmentId, String departmentName) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
    }

    public Departments(String departmentName, Employees departmentBossid) {
        this.departmentName = departmentName;
        this.departmentBossid = departmentBossid;
    }

    public Departments(int departmentId, String departmentName, Employees departmentBossid) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.departmentBossid = departmentBossid;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @XmlTransient
    public Collection<Employees> getEmployeesCollection() {
        return employeesCollection;
    }

    public void setEmployeesCollection(Collection<Employees> employeesCollection) {
        this.employeesCollection = employeesCollection;
    }

    public Employees getDepartmentBossid() {
        return departmentBossid;
    }

    public void setDepartmentBossid(Employees departmentBossid) {
        this.departmentBossid = departmentBossid;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + this.departmentId;
        hash = 11 * hash + Objects.hashCode(this.departmentName);
        hash = 11 * hash + Objects.hashCode(this.employeesCollection);
        hash = 11 * hash + Objects.hashCode(this.departmentBossid);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Departments other = (Departments) obj;
        if (this.departmentId != other.departmentId) {
            return false;
        }
        if (!Objects.equals(this.departmentName, other.departmentName)) {
            return false;
        }
        if (!Objects.equals(this.employeesCollection, other.employeesCollection)) {
            return false;
        }
        if (!Objects.equals(this.departmentBossid, other.departmentBossid)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Departments{" + "departmentId=" + departmentId + ", departmentName=" + departmentName + ", employeesCollection=" + employeesCollection + ", departmentBossid=" + departmentBossid + '}';
    }

}
