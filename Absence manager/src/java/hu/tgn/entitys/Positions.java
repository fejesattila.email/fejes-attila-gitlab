package hu.tgn.entitys;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "POSITIONS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Positions.findAll", query = "SELECT p FROM Positions p")
    , @NamedQuery(name = "Positions.findByPositionId", query = "SELECT p FROM Positions p WHERE p.positionId = :positionId")
    , @NamedQuery(name = "Positions.findByPositionName", query = "SELECT p FROM Positions p WHERE p.positionName = :positionName")
    , @NamedQuery(name = "Positions.findByDangerous", query = "SELECT p FROM Positions p WHERE p.dangerous = :dangerous")})
public class Positions implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "s_positions", allocationSize = 1)
    @GeneratedValue(generator = "s_positions", strategy = GenerationType.SEQUENCE)

    @Basic(optional = false)
    @Column(name = "POSITION_ID")
    private int positionId;
    @Basic(optional = false)
    @Column(name = "POSITION_NAME")
    private String positionName;

    @Basic(optional = false)
    @Column(name = "DANGEROUS")
    private int dangerous;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "positionId")
    private Collection<Employees> employeesCollection;
    @Basic(optional = false)
    @Column(name = "ROLE")
    private String role;

    public Positions() {
    }

    public Positions(int positionId) {
        this.positionId = positionId;
    }

    public Positions(String positionName, int dangerous, String role) {
        this.positionName = positionName;
        this.dangerous = dangerous;
        this.role = role;
    }

    public Positions(String positionName, int dangerous) {
        this.positionName = positionName;
        this.dangerous = dangerous;
    }

    public Positions(int positionId, String positionName, int dangerous) {
        this.positionId = positionId;
        this.positionName = positionName;
        this.dangerous = dangerous;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public int getDangerous() {
        return dangerous;
    }

    public void setDangerous(int dangerous) {
        this.dangerous = dangerous;
    }

    @XmlTransient
    public Collection<Employees> getEmployeesCollection() {
        return employeesCollection;
    }

    public void setEmployeesCollection(Collection<Employees> employeesCollection) {
        this.employeesCollection = employeesCollection;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.positionId;
        hash = 53 * hash + Objects.hashCode(this.positionName);
        hash = 53 * hash + this.dangerous;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Positions other = (Positions) obj;
        if (this.positionId != other.positionId) {
            return false;
        }
        if (this.dangerous != other.dangerous) {
            return false;
        }
        if (!Objects.equals(this.positionName, other.positionName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Positions{" + "positionId=" + positionId + ", positionName=" + positionName + ", dangerous=" + dangerous + ", employeesCollection=" + employeesCollection + '}';
    }

}
