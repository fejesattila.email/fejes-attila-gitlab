package hu.tgn.services;

import hu.tgn.dtos.PositionsDTO;
import java.util.List;

public interface PositionService {

    public int insertPosition(PositionsDTO pdto);

    public PositionsDTO positionByID(int positionID);

    public int modifyPosition(PositionsDTO pdto);

    void deletePosition(PositionsDTO pdto);

    public List<PositionsDTO> allPositions();

}
