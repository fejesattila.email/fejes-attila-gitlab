package hu.tgn.services;

import hu.tgn.dtos.AbsenceRequestDTO;
import java.util.List;

public interface AbsenceRequestService {

    public int insertAbsenceRequest(AbsenceRequestDTO ardto);

    public int modifyAbsenceRequest(AbsenceRequestDTO ardto);

    public int deleteAbsenceRequest(AbsenceRequestDTO ardto);

    public List<AbsenceRequestDTO> allPendingAbsenceRequest();

    public List<AbsenceRequestDTO> allAbsenceRequestByEmployee(int employeeID);

    public List<AbsenceRequestDTO> allAbsenceRequestByEmployeeAndAbsenceType(int employeeID, String absenceType);

    public List<AbsenceRequestDTO> allAbsenceRequestByEmployeeAndAbsenceStatus(int employeeID, String absenceStatus);

    public int absenceRequestApproved(AbsenceRequestDTO ardto);

    public int absenceRequestRejected(AbsenceRequestDTO ardto);

}
