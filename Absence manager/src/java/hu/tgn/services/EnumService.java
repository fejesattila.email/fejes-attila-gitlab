package hu.tgn.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface EnumService {

    public void addAbsenceStatus(String str);

    public void addAbsenceType(String str);

    public void addRole(String str);

    public void addRequestType(String str);

    public void addDate(Date date);

    public void addStandardVacationDays(int age, int vacationNumber);

    public void addSpecialVacationDays(String name, int vacationNumber);

    public List<String> getAllRole();

    public List<String> getAllAbsenceStatus();

    public List<String> getAllAbsenceType();

    public List<String> getAllRequestType();

    public List<Date> getAllDate();

    public HashMap<Integer, Integer> vacationDayByAge();

    public int vacationDayByAge(int age);

    public int specialVacationDayByName(String name);
}
