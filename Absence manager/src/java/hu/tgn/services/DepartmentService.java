package hu.tgn.services;

import hu.tgn.dtos.DepartmentsDTO;
import hu.tgn.dtos.EmployeesDTO;

import java.util.HashSet;
import java.util.List;

public interface DepartmentService {

    public int insertDepartment(DepartmentsDTO depdto);

    public int modifyDepartment(DepartmentsDTO depdto);

    void deleteDepartment(DepartmentsDTO depdto);

    public List<DepartmentsDTO> allDepartments();

}
