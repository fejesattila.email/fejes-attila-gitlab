package hu.tgn.services;

import hu.tgn.dtos.ChildrenDTO;

public interface ChildService {

    public int insertChild(ChildrenDTO cdto);

    public int modifyChild(ChildrenDTO cdto);

    void delete(ChildrenDTO cdto);

    public int vacationDaysAfterChildren(int parentID);

}
