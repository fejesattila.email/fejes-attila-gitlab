package hu.tgn.data;

import hu.tgn.beans.DateConverterBean;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReadOutDays {

    static TreeSet<Integer> workOutDays = new TreeSet<>();
    static DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    public static void main(String[] args) {
        loadWorkOutDays();
        for (Integer i : workOutDays) {
            System.out.println(i);
        }
    }

    public static void loadWorkOutDays() {
        File file = new File("./src/java/hu/tgn/data/2018OUTDAYS.txt");
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = br.readLine()) != null) {
                workOutDays.add((int) (formatter.parse(line).getTime() / 86400000));
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(DateConverterBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }

//        File file = new File("./src/java/hu/tgn/data/2018OUTDAYS.txt");
//        BufferedReader br = null;
//        List<String> dates = new ArrayList<>();
//        try {
//            br = new BufferedReader(new FileReader(file));
//            String line = null;
//            while ((line = br.readLine()) != null) {
//                dates.add(line);
//            }
//        } catch (FileNotFoundException ex) {
//            System.out.println(ex.getMessage());
//        } catch (IOException ex) {
//            System.out.println(ex.getMessage());
//        } finally {
//            try {
//                br.close();
//            } catch (IOException ex) {
//                System.out.println(ex.getMessage());
//            }
//        }
    }
}
