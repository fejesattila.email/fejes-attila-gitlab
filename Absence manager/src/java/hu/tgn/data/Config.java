package hu.tgn.data;

public interface Config {

    int BASENUMBEROFVACATION = 20;
    int CHILDUNDER17 = 17;
    int ONECHILD = 2;
    int TWOCHILDREN = 4;
    int MORETHANTWOCHILD = 7;
    int HANDICAPPEDCHILD = 2;
    int EMPLOYEEHANDICAPPED = 5;
    int UNDER18 = 5;
    int DANGEROUSCIRCUMSTANCE = 5;
    int[][] EXTRADAYSBYAGE
            = {{25, 1},
            {28, 2},
            {31, 3},
            {33, 4},
            {35, 5},
            {37, 6},
            {39, 7},
            {41, 8},
            {43, 9},
            {45, 10}};
}
