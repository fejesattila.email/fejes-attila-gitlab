package hu.tgn.servlets;

import hu.tgn.beans.AbsenceRequestBean;
import hu.tgn.dtos.AbsenceRequestDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AbsenceServlet extends HttpServlet {

    @EJB
    private AbsenceRequestBean absenceRequestBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);

        if (null == session.getAttribute("loginSessionBean")) {

            response.sendRedirect("login");

        } else {
            request.getRequestDispatcher("WEB-INF/absencejsp.jsp").forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        LoginSessionBean loginSessionBean = (LoginSessionBean) session.getAttribute("loginSessionBean");
        int userId = 0;
        try {
            userId = loginSessionBean.getLoggedUser().getId();
        } catch (NullPointerException ex) {
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        }

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date absenceStartDay = null;
        Date absenceEndDay = null;

        try {
            absenceStartDay = formatter.parse(request.getParameter("absencestartdate"));
            absenceEndDay = formatter.parse(request.getParameter("absenceenddate"));

        } catch (ParseException ex) {
            Logger.getLogger(AbsenceServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        AbsenceRequestDTO ardto = new AbsenceRequestDTO(
                absenceStartDay,
                absenceEndDay,
                request.getParameter("absencetype"),
                "PENDING",
                "NEW",
                userId);
        int arNum = absenceRequestBean.insertAbsenceRequest(ardto);
        System.out.println("INFO: _______  _________  REQUEST NUMBER: " + arNum);
        try (PrintWriter out = response.getWriter()) {
            out.println("<h1>Request registered: " + arNum + "</h1>");
            out.println(" <a href=\"/AbsenceEJB/AbsenceServlet\">Back</a> ");
        }
    }

}
