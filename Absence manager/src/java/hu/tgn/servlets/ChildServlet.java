package hu.tgn.servlets;

import hu.tgn.beans.ChildBean;
import hu.tgn.beans.EmployeeBean;
import hu.tgn.dtos.ChildrenDTO;
import hu.tgn.dtos.EmployeesDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChildServlet extends HttpServlet {

    @EJB
    private ChildBean childBean;

    @EJB
    private EmployeeBean employeeBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<EmployeesDTO> dlist = employeeBean.allEmployees();
        List<String> listWithNames = new ArrayList<>();
        for (EmployeesDTO parent : dlist) {

            listWithNames.add(String.format("%03d", parent.getEmployeeId()) + " " + parent.getFirstname() + " " + parent.getLastname());
        }

        request.setAttribute("list", listWithNames);
        request.getRequestDispatcher("WEB-INF/childjsp.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date birthDay = null;
        try {
            birthDay = formatter.parse(request.getParameter("birthday"));
        } catch (ParseException ex) {
            Logger.getLogger(EmployeeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        ChildrenDTO cdto = new ChildrenDTO(
                birthDay,
                Integer.parseInt(request.getParameter("handicapped")),
                Integer.parseInt(request.getParameter("parentId").substring(0, 3)));

        int childNum = childBean.insertChild(cdto);

        System.out.println("INFO: ____________________  CHILD NUMBER: " + childNum);
        try (PrintWriter out = response.getWriter()) {
            out.println("<h1>Child registered: " + childNum + "</h1>");
            out.println(" <a href=\"/AbsenceEJB/ChildServlet\">Back</a> ");
        }

        //response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));
    }
}
