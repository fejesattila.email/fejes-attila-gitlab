package hu.tgn.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //HttpSession session = request.getSession(false);
        //LoginSessionBean loginSessionBean = (LoginSessionBean) session.getAttribute("loginSessionBean");
//        if (session != null) {
//            //loginSessionBean.setLoggedOut(true);
//            session.invalidate();
//            //request.setAttribute("message", "Sikeres kijelentkezés!");
//            response.sendRedirect("login");
//        }
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
            response.sendRedirect("login");
        }

    }
}
