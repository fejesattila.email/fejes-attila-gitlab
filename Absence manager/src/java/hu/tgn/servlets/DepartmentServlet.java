package hu.tgn.servlets;

import hu.tgn.beans.DepartmentBean;
import hu.tgn.beans.EmployeeBean;
import hu.tgn.dtos.DepartmentsDTO;
import hu.tgn.dtos.EmployeesDTO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DepartmentServlet extends HttpServlet {

    @EJB
    private DepartmentBean departmentBean;

    @EJB
    private EmployeeBean employeeBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<EmployeesDTO> dlist = employeeBean.allEmployees();

        List<String> listWithNames = new ArrayList<>();

        for (EmployeesDTO boss : dlist) {
            listWithNames.add(String.format("%03d", boss.getEmployeeId()) + " " + boss.getFirstname() + " " + boss.getLastname());
        }
        System.out.println(dlist);
        request.setAttribute("list", listWithNames);

        request.getRequestDispatcher("WEB-INF/departmentjsp.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DepartmentsDTO ddto = new DepartmentsDTO(request.getParameter("departmentname"), Integer.valueOf(request.getParameter("bossname").substring(0, 3)));
        int depNum = departmentBean.insertDepartment(ddto);
        System.out.println("INFO: _______  _________  DEPARTMENT NUMBER: " + depNum);
        try (PrintWriter out = response.getWriter()) {
            out.println("<h1>Department registered: " + depNum + "</h1>");
            out.println(" <a href=\"/AbsenceEJB/DepartmentServlet\">Back</a> ");
        }
    }
}
