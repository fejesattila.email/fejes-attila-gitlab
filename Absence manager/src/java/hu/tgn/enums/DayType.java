package hu.tgn.enums;

public enum DayType {
    HOLIDAY(4),
    WORKDAY(3),
    EXTRAWORKDAY(2),
    RESTDAY(1);

    private final int dayType;

    private DayType(int daytype) {
        this.dayType = daytype;
    }

    public int getDayType() {
        return dayType;
    }

}
