package hu.tgn.enums;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "WODAYS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WorkoutDays.findAll", query = "SELECT w FROM WorkoutDays w")
    , @NamedQuery(name = "WorkoutDays.findById", query = "SELECT w FROM WorkoutDays w WHERE w.id = :id")
    , @NamedQuery(name = "WorkoutDays.findByWoDate", query = "SELECT w FROM WorkoutDays w WHERE w.woDate = :woDate")})
public class WorkoutDays implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID")
    private int id;
    @Column(name = "WO_DATE")
    @Temporal(TemporalType.DATE)
    private Date woDate;

    public WorkoutDays() {
    }

    public WorkoutDays(int id) {
        this.id = id;
    }

    public WorkoutDays(Date woDate) {
        this.woDate = woDate;
    }

    public WorkoutDays(int id, Date woDate) {
        this.id = id;
        this.woDate = woDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getWoDate() {
        return woDate;
    }

    public void setWoDate(Date woDate) {
        this.woDate = woDate;
    }

}
