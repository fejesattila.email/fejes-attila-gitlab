package hu.tgn.enums;

public enum AbsenceType {
    GENERAL(1),
    MATERNITY(2),
    SICKLEAVE(3),
    OTHER(4),
    UNPAIDLEAVE(5),
    STUDY(6),
    HOMEOFFICE(7),
    SECONDMENT(8),
    BUSINESSTRIP(9);

    private final int absenceType;

    private AbsenceType(int absenceType) {
        this.absenceType = absenceType;
    }

    public int getAbsenceType() {
        return absenceType;
    }

}
