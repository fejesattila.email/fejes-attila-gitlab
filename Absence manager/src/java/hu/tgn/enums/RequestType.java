package hu.tgn.enums;

public enum RequestType {
    NEW(3),
    MODIFY(2),
    DELETE(1);

    private final int requestType;

    private RequestType(int requestType) {
        this.requestType = requestType;
    }

    public int getRequestType() {
        return requestType;
    }
}
