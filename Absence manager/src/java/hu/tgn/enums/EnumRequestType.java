package hu.tgn.enums;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ENUM_REQUEST_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EnumRequestType.findAll", query = "SELECT e FROM EnumRequestType e")
    , @NamedQuery(name = "EnumRequestType.findById", query = "SELECT e FROM EnumRequestType e WHERE e.id = :id")
    , @NamedQuery(name = "EnumRequestType.findByName", query = "SELECT e FROM EnumRequestType e WHERE e.name = :name")})
public class EnumRequestType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID")
    private int id;
    @Column(name = "NAME")
    private String name;

    public EnumRequestType() {
    }

    public EnumRequestType(int id) {
        this.id = id;
    }

    public EnumRequestType(String name) {
        this.name = name;
    }

    public EnumRequestType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EnumRequestType other = (EnumRequestType) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EnumRequestType{" + "id=" + id + ", name=" + name + '}';
    }

}
