package hu.tgn.enums;

public enum AbsenceStatus {
    PENDING(1),
    APPROVED(2),
    REJECTED(3);

    private final int absenceStatus;

    private AbsenceStatus(int absenceStatus) {
        this.absenceStatus = absenceStatus;
    }

    public int getAbsenceStatus() {
        return absenceStatus;
    }

}
