package hu.tgn.enums;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ENUM_ABSENCE_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EnumAbsenceType.findAll", query = "SELECT e FROM EnumAbsenceType e")
    , @NamedQuery(name = "EnumAbsenceType.findById", query = "SELECT e FROM EnumAbsenceType e WHERE e.id = :id")
    , @NamedQuery(name = "EnumAbsenceType.findByName", query = "SELECT e FROM EnumAbsenceType e WHERE e.name = :name")})
public class EnumAbsenceType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID")
    private int id;
    @Column(name = "NAME")
    private String name;

    public EnumAbsenceType() {
    }

    public EnumAbsenceType(int id) {
        this.id = id;
    }

    public EnumAbsenceType(String name) {
        this.name = name;
    }

    public EnumAbsenceType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.id;
        hash = 41 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EnumAbsenceType other = (EnumAbsenceType) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EnumAbsenceType{" + "id=" + id + ", name=" + name + '}';
    }

}
