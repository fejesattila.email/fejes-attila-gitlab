package hu.tgn.enums;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "SPECIALVACATIONDAYS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Specialvacationdays.findAll", query = "SELECT s FROM Specialvacationdays s")
    , @NamedQuery(name = "Specialvacationdays.findByName", query = "SELECT s FROM Specialvacationdays s WHERE s.name = :name")
    , @NamedQuery(name = "Specialvacationdays.findByDays", query = "SELECT s FROM Specialvacationdays s WHERE s.days = :days")})
public class Specialvacationdays implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "s_specialvacationdays", sequenceName = "s_specialvacationdays", allocationSize = 1)
    @GeneratedValue(generator = "s_specialvacationdays", strategy = GenerationType.SEQUENCE)

    @Basic(optional = false)
    @Column(name = "ID")
    private int id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "DAYS")
    private int days;

    public Specialvacationdays() {
    }

    public Specialvacationdays(String name, int days) {
        this.name = name;
        this.days = days;
    }

    public Specialvacationdays(int id, String name, int days) {
        this.id = id;
        this.name = name;
        this.days = days;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + this.days;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Specialvacationdays other = (Specialvacationdays) obj;
        if (this.days != other.days) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Specialvacationdays{" + "name=" + name + ", days=" + days + '}';
    }

}
