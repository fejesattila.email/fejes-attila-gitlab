package hu.tgn.enums;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ENUM_ROLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EnumRole.findAll", query = "SELECT e FROM EnumRole e")
    , @NamedQuery(name = "EnumRole.findById", query = "SELECT e FROM EnumRole e WHERE e.id = :id")
    , @NamedQuery(name = "EnumRole.findByName", query = "SELECT e FROM EnumRole e WHERE e.name = :name")})
public class EnumRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "ID")
    private int id;
    @Column(name = "NAME")
    private String name;

    public EnumRole() {
    }

    public EnumRole(int id) {
        this.id = id;
    }

    public EnumRole(String name) {
        this.name = name;
    }

    public EnumRole(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.id;
        hash = 43 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EnumRole other = (EnumRole) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EnumRole{" + "id=" + id + ", name=" + name + '}';
    }

}
