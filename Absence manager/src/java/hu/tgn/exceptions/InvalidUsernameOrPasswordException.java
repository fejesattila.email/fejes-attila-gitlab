package hu.tgn.exceptions;

public class InvalidUsernameOrPasswordException extends Exception {
    
    @Override
    public String getMessage(){
        return "Login denied! Invalid username or password!";
    }
    
}
