package hu.tgn.beans;

import hu.tgn.dtos.UserDTO;
import hu.tgn.exceptions.InvalidUsernameOrPasswordException;
import hu.tgn.exceptions.ReachedMaximumLoginAttemptsException;
import hu.tgn.exceptions.UserDoesNotExistException;
import hu.tgn.services.LoginService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class LoginServiceBean implements LoginService {
    
    @EJB
    private UserBean userBean;
    
    @PersistenceContext
    EntityManager em;
        
    @Override
    public UserDTO login(String username, String password, String sessionID) 
        throws InvalidUsernameOrPasswordException, ReachedMaximumLoginAttemptsException, UserDoesNotExistException {
        UserDTO user = userBean.getUserByUsername(username);
        if (user == null){
            System.out.println("Nincs ilyen User");
            throw new UserDoesNotExistException();
        }
        if (user.getPassword().equals(password)){
            System.out.println("Van user és a jelszava is oké");
            user.setLoggedIn(1);
            user.setSessionId(sessionID);
            userBean.updateUser(user);
        } else {
            System.out.println("Van user, de nem stimmel a jelszó");
            if (user.getLoginAttempts() == 3){
                // Setting the user inactive(banned) after 3 failed login attempt (admin can reset password and active status)
                // TODO: make it possible to ban users for 15 minutes before the next login attempt
                user.setStatus(0);
                userBean.updateUser(user);
                throw new ReachedMaximumLoginAttemptsException();
            } else {
                user.setLoginAttempts(user.getLoginAttempts()+1);
                userBean.updateUser(user);
                throw new InvalidUsernameOrPasswordException();
            }
        }
        return user;
    }
    
    @Override
    public UserDTO getUserData(Integer id){
        return (UserDTO)userBean.getUserById(id);
    }

    @Remove
    @Override
    public void logout(Integer id) {
        UserDTO user = userBean.getUserById(id);
        user.setLoggedIn(0);
        userBean.updateUser(user);
    }
    
    @Override
    public boolean changePassword(Integer id, String newPassword){
        try {
            UserDTO user = userBean.getUserById(id);
            user.setPassword(newPassword);
            user.setFirstLogin(1);
            user.setPasswordChanged(0);
            userBean.updateUser(user);
            return true;
        } catch(NoResultException | EJBException ex){
            return false;
        }
    }
    
}