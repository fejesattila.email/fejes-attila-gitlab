package hu.tgn.beans;

import hu.tgn.dtos.AbsencesDTO;
import hu.tgn.dtos.EmployeesDTO;
import hu.tgn.entitys.AbsenceRequest;
import hu.tgn.entitys.Absences;
import hu.tgn.entitys.Employees;
import hu.tgn.mapper.AbsenceMapper;
import hu.tgn.mapper.EmployeeMapper;
import hu.tgn.services.AbsenceService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
@Remote(AbsenceService.class)
public class AbsenceBean implements AbsenceService {

    @EJB
    private EmployeeMapper employeeMapper;

    @EJB
    private AbsenceMapper absenceMapper;

    @PersistenceContext
    private EntityManager em;

    @Override
    public int insertAbsence(AbsencesDTO adto) {
        Absences absences = absenceMapper.DTO2Entity(adto);
        em.persist(absences);
        return absences.getAbsenceId();
    }

    @Override
    public int modifyAbsence(AbsencesDTO adto) {
        return em.createQuery("UPDATE Absences a SET "
                + "a.absenceStatus =:status, "
                + "a.absenceType =:type "
                + "WHERE a.absenceId =:id", Absences.class)
                .setParameter("status", adto.getAbsenceStatus())
                .setParameter("type", adto.getAbsenceType())
                .setParameter("id", adto.getAbsenceId())
                .executeUpdate();
    }

    @Override
    public void deleteAbsence(AbsencesDTO adto) {
        em.createQuery("DELETE FROM Absences a WHERE a.absenceId =:id", Absences.class)
                .setParameter("id", adto.getAbsenceId())
                .executeUpdate();
    }

    @Override
    public List<EmployeesDTO> allEmployeesOutOfOffice(Date today) {
        List<Employees> es = em.createQuery("SELECT DISTINCT e FROM Absences a INNER JOIN a.employeeId e WHERE a.absenceDay=:today", Employees.class)
                .setParameter("today", today).getResultList();
        List<EmployeesDTO> edtos = new ArrayList<>();
        for (Employees e : es) {
            edtos.add(employeeMapper.Entity2DTO(e));
        }
        return edtos;
    }

    @Override
    public int numberOfAbsenceDays(int employee, String absenceType) {
        List<Absences> absence = em.createQuery("SELECT a FROM Absences a WHERE a.employeeId =:emp AND a.absenceType =:abs", Absences.class)
                .setParameter("emp", new Employees(employee))
                .setParameter("abs", absenceType)
                .getResultList();
        return absence.size();
    }

    @Override
    public List<AbsencesDTO> allAbsenceByAbsenceReqest(int absenceRequest) {
        List<Absences> absences = em.createQuery("SELECT a FROM Absences a WHERE a.absenceRequest =:abs", Absences.class)
                .setParameter("abs", new AbsenceRequest(absenceRequest))
                .getResultList();
        List<AbsencesDTO> adtos = new ArrayList<>();
        for (Absences a : absences) {
            adtos.add(absenceMapper.Entity2DTO(a));
        }
        return adtos;
    }

    @Override
    public void deleteAbsenceaByAbsenceReqest(int absenceRequest) {
        for (AbsencesDTO adto : allAbsenceByAbsenceReqest(absenceRequest)) {
            deleteAbsence(adto);
        }
    }

    @Override
    public List<AbsencesDTO> allAbsenceByEmployee(int emoloyeeID) {
        List<Absences> absences = em.createQuery("SELECT a FROM Absences a WHERE a.employeeId =:emp", Absences.class)
                .setParameter("emp", new Employees(emoloyeeID))
                .getResultList();
        List<AbsencesDTO> adtos = new ArrayList<>();
        for (Absences a : absences) {
            adtos.add(absenceMapper.Entity2DTO(a));
        }
        return adtos;
    }
}
