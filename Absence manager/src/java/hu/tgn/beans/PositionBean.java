package hu.tgn.beans;

import hu.tgn.dtos.PositionsDTO;
import hu.tgn.entitys.Positions;
import hu.tgn.mapper.PositionMapper;
import hu.tgn.services.PositionService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
@Remote(PositionService.class)
public class PositionBean implements PositionService {

    @EJB
    private PositionMapper pMapper;

    @PersistenceContext
    private EntityManager em;

    @Override
    public int insertPosition(PositionsDTO pdto) {
        Positions positions = pMapper.DTO2Entity(pdto);
        em.persist(positions);
        return positions.getPositionId();

    }

    @Override
    public PositionsDTO positionByID(int positionID) {
        Positions p = null;
        try {
            p = (Positions) em.createNamedQuery("Positions.findByPositionId").setParameter("positionId", positionID).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return pMapper.Entity2DTO(p);
    }

    @Override
    public int modifyPosition(PositionsDTO pdto) {
        return em.createQuery("UPDATE Positions p SET p.positionName = '" + pdto.getPositionName()
                + "', p.dangerous = " + pdto.getDangerous()
                + " WHERE p.positionId=:id", Positions.class)
                .setParameter("id", pdto.getPositionId()).executeUpdate();
    }

    @Override
    public void deletePosition(PositionsDTO pdto) {
        em.createQuery("DELETE FROM Positions p WHERE p.positionId =:id", Positions.class)
                .setParameter("id", pdto.getPositionId())
                .executeUpdate();
    }

    @Override
    public List<PositionsDTO> allPositions() {
        List<Positions> allPositions = em.createNamedQuery("Positions.findAll").getResultList();
        List<PositionsDTO> pdtos = new ArrayList<>();
        for (Positions p : allPositions) {
            pdtos.add(pMapper.Entity2DTO(p));
        }
        return pdtos;
    }
}
