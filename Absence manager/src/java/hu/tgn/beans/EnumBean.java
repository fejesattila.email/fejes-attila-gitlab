package hu.tgn.beans;

import hu.tgn.enums.EnumAbsenceStatus;
import hu.tgn.enums.EnumAbsenceType;
import hu.tgn.enums.EnumRequestType;
import hu.tgn.enums.EnumRole;
import hu.tgn.enums.Specialvacationdays;
import hu.tgn.enums.Standardvacationdays;
import hu.tgn.enums.WorkoutDays;
import hu.tgn.services.EnumService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
@LocalBean
@Remote(EnumService.class)
public class EnumBean implements EnumService {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void addAbsenceStatus(String str) {
        em.persist(new EnumAbsenceStatus(str));
    }

    @Override
    public void addAbsenceType(String str) {
        em.persist(new EnumAbsenceType(str));
    }

    @Override
    public void addRole(String str) {
        em.persist(new EnumRole(str));
    }

    @Override
    public void addRequestType(String str) {
        em.persist(new EnumRequestType(str));
    }

    @Override
    public void addDate(Date date) {
        em.persist(new WorkoutDays(date));
    }

    @Override
    public void addStandardVacationDays(int age, int vacationNumber) {
        em.persist(new Standardvacationdays(age, vacationNumber));
    }

    @Override
    public void addSpecialVacationDays(String name, int vacationNumber) {
        em.persist(new Specialvacationdays(name, vacationNumber));
    }

    @Override
    public List<String> getAllRole() {
        List<String> list = new ArrayList<>();
        TypedQuery<EnumRole> query = em.createNamedQuery("EnumRole.findAll", EnumRole.class);
        for (EnumRole role : query.getResultList()) {
            list.add(role.getName());
        }
        return list;
    }

    @Override
    public List<String> getAllAbsenceStatus() {
        List<String> list = new ArrayList<>();
        List<EnumAbsenceStatus> eases = em.createNamedQuery("EnumAbsenceStatus.findAll", EnumAbsenceStatus.class).getResultList();
        for (EnumAbsenceStatus eas : eases) {
            list.add(eas.getName());
        }
        return list;
    }

    @Override
    public List<String> getAllAbsenceType() {
        List<String> list = new ArrayList<>();
        List<EnumAbsenceType> eates = em.createNamedQuery("EnumAbsenceType.findAll", EnumAbsenceType.class).getResultList();
        for (EnumAbsenceType eat : eates) {
            list.add(eat.getName());
        }
        return list;
    }

    @Override
    public List<String> getAllRequestType() {
        List<String> list = new ArrayList<>();
        List<EnumRequestType> ertes = em.createNamedQuery("EnumRequestType.findAll", EnumRequestType.class).getResultList();
        for (EnumRequestType ert : ertes) {
            list.add(ert.getName());
        }
        return list;
    }

    @Override
    public List<Date> getAllDate() {
        List<WorkoutDays> wds = em.createNamedQuery("WorkoutDays.findAll", WorkoutDays.class).getResultList();
        List<Date> dates = new ArrayList<>();
        for (WorkoutDays wd : wds) {
            dates.add(wd.getWoDate());
        }
        return dates;
    }

    @Override
    public HashMap<Integer, Integer> vacationDayByAge() {
        List<Standardvacationdays> ses = em.createNamedQuery("Standardvacationdays.findAll").getResultList();
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (Standardvacationdays vac : ses) {
            map.put(vac.getAge(), vac.getDays());
        }
        return map;
    }

    @Override
    public int vacationDayByAge(int age) {
        Standardvacationdays s = (Standardvacationdays) em.createNamedQuery("Standardvacationdays.findByAge").setParameter("age", age).getSingleResult();
        return s.getDays();
    }

    @Override
    public int specialVacationDayByName(String name) {
        Specialvacationdays s = (Specialvacationdays) em.createNamedQuery("Specialvacationdays.findByName").setParameter("name", name).getSingleResult();
        return s.getDays();
    }

}
