package hu.tgn.beans;

import hu.tgn.dtos.ChildrenDTO;
import hu.tgn.entitys.Children;
import hu.tgn.entitys.Employees;
import hu.tgn.data.Config;
import hu.tgn.mapper.ChildMapper;
import hu.tgn.services.ChildService;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
@LocalBean
@Remote(ChildService.class)
public class ChildBean implements ChildService {

    @EJB
    private EnumBean enumBean;

    @EJB
    private ChildMapper childMapper;

    @PersistenceContext
    private EntityManager em;

    @Override
    public int insertChild(ChildrenDTO cdto) {
        Children child = childMapper.DTO2Entity(cdto);
        em.persist(child);
        return child.getChildId();
    }

    @Override
    public int vacationDaysAfterChildren(int parentID) {
        int vacationDays = 0;
        Employees e = new Employees(parentID);
        TypedQuery<Children> parent1 = em.createQuery("SELECT c FROM Children c WHERE c.parentId =:parentID", Children.class).setParameter("parentID", e);
        TypedQuery<Children> parent2 = em.createQuery("SELECT c FROM Children c WHERE c.parent2Id =:parentID", Children.class).setParameter("parentID", e);
        int numOfChildren = numOfChildUnderAge(parent1) + numOfChildUnderAge(parent2);
        int handicappedChildren = numOfHandicappedChild(parent1) + numOfHandicappedChild(parent2);
        if (numOfChildren == 1) {
            vacationDays += enumBean.specialVacationDayByName("ONECHILD");
        } else if (numOfChildren == 2) {
            vacationDays += enumBean.specialVacationDayByName("TWOCHILDREN");
        } else if (numOfChildren > 2) {
            vacationDays += enumBean.specialVacationDayByName("MORETHANTWOCHILD");
        }
        vacationDays += (handicappedChildren * enumBean.specialVacationDayByName("HANDICAPPEDCHILD"));
        return vacationDays;
    }

    private int numOfChildUnderAge(TypedQuery<Children> typedQuery) {
        int underAge = enumBean.specialVacationDayByName("CHILDUNDER17");
        //int underAge = Config.CHILDUNDER17;
        Date today = new Date(System.currentTimeMillis());

        int actualYear = yearOfdate(today);
        if (typedQuery != null) {
            int numOfChildUnderAge = 0;
            for (Children child : typedQuery.getResultList()) {
                int childBirthYear = yearOfdate(child.getChildBirthday());
                if (actualYear - childBirthYear < underAge) {
                    numOfChildUnderAge++;
                }
            }
            return numOfChildUnderAge;
        }
        return 0;
    }

    private int yearOfdate(Date date) {
        SimpleDateFormat dateYearFormat = new SimpleDateFormat("yyyy");
        return Integer.parseInt(dateYearFormat.format(date));
    }

    private int numOfHandicappedChild(TypedQuery<Children> typedQuery) {
        int handicappedChildren = 0;
        if (typedQuery != null) {
            for (Children child : typedQuery.getResultList()) {
                if (child.getHandicapped() == 1) {
                    handicappedChildren++;
                }
            }
        }
        return handicappedChildren;
    }

    @Override
    public int modifyChild(ChildrenDTO cdto) {
        return em.createQuery("UPDATE Children c SET "
                + "c.childBirthday =:birthday, "
                + "c.handicapped =:hCapped, "
                + "c.parentId =:parent, "
                + "c.parent2Id =:parent2 "
                + "WHERE c.childId =:id", Children.class)
                .setParameter("birthday", cdto.getBirthday())
                .setParameter("hCapped", cdto.getHandicapped())
                .setParameter("parent", new Employees(cdto.getParentId()))
                .setParameter("parent2", new Employees(cdto.getParent2Id()))
                .setParameter("id", cdto.getChildId())
                .executeUpdate();
    }

    @Override
    public void delete(ChildrenDTO cdto) {
        em.createQuery("DELETE FROM Children c WHERE c.childId =:id", Children.class)
                .setParameter("id", cdto.getChildId())
                .executeUpdate();
    }
}
