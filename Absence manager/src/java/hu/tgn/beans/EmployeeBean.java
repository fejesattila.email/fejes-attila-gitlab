package hu.tgn.beans;

import hu.tgn.dtos.EmployeesDTO;
import hu.tgn.entitys.Departments;
import hu.tgn.entitys.Employees;
import hu.tgn.entitys.Positions;
import hu.tgn.mapper.EmployeeMapper;
import hu.tgn.services.EmployeeService;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
@LocalBean
@Remote(EmployeeService.class)
public class EmployeeBean implements EmployeeService {
    
    @EJB
    private UserBean userBean;
    
    @EJB
    private EnumBean enumBean;
    
    @EJB
    private ChildBean childBean;
    
    @Inject
    EmployeeMapper employeeMapper;
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public int insertEmployee(EmployeesDTO edto) {
        Employees employee = employeeMapper.DTO2Entity(edto);
        em.persist(employee);
        userBean.insertUser(employeeMapper.Entity2DTO(employee));
        return employee.getEmployeeId();
    }
    
    @Override
    public EmployeesDTO getEmployeeById(int id) {
        return employeeMapper.Entity2DTO(em.find(Employees.class, id));
    }
    
    @Override
    public List<EmployeesDTO> getEmployeesByName(String name) {
        List<Employees> employeeses = em.createQuery("SELECT e FROM Employees e WHERE LOCATE(:name, e.firstname) > 0", Employees.class).setParameter("name", name).getResultList();
        List<EmployeesDTO> employeesDTOs = new ArrayList<>();
        for (Employees emp : employeeses) {
            employeesDTOs.add(employeeMapper.Entity2DTO(emp));
        }
        return employeesDTOs;
    }
    
    @Override
    public int modifyEmployee(EmployeesDTO edto) {
        return em.createQuery("UPDATE Employees e SET "
                + "e.firstname =:firstname, "
                + "e.lastname =:lastname, "
                + "e.birthday =:birthday, "
                + "e.contractstyle =:contractstyle, "
                + "e.departmentId =:departmentId, "
                + "e.email =:email, "
                + "e.startofwork =:startofwork, "
                + "e.endofwork =:endofwork, "
                + "e.positionId =:positionId, "
                + "c.handicapped =:handicapped, "
                + "e.remaininglastyearvacations =:remaininglastyearvacations "
                + "WHERE e.employeeId =:id", Employees.class)
                .setParameter("firstname", edto.getFirstname())
                .setParameter("lastname", edto.getLastname())
                .setParameter("birthday", edto.getBirthday())
                .setParameter("contractstyle", edto.getContractstyle())
                .setParameter("departmentId", edto.getDepartmentID())
                .setParameter("email", edto.getEmail())
                .setParameter("startofwork", edto.getStartofwork())
                .setParameter("endofwork", edto.getEndofwork())
                .setParameter("positionId", new Positions(edto.getPositionID()))
                .setParameter("handicapped", edto.getHandicapped())
                .setParameter("remaininglastyearvacations", edto.getRemaininglastyearvacations())
                .setParameter("id", edto.getEmployeeId())
                .executeUpdate();
    }
    
    @Override
    public void delete(EmployeesDTO edto) {
        em.createQuery("DELETE FROM Employees e WHERE e.employeeId =:id", Employees.class)
                .setParameter("id", edto.getEmployeeId())
                .executeUpdate();
    }
    
    @Override
    public int numberOfVacationThisYear(int id) {
        int numberOfVacationByYear = 0;
        
        Employees emp = em.find(Employees.class, id);
        int age = ageOfEmployee(emp.getBirthday());

//standard-vacationsdays
        if (age < 25) {
            numberOfVacationByYear += enumBean.vacationDayByAge(24);
        } else if (age > 44) {
            numberOfVacationByYear += enumBean.vacationDayByAge(45);
        } else {
            numberOfVacationByYear += enumBean.vacationDayByAge(age);
        }
//bonus-vacationdays
        if (age < 18) {
            numberOfVacationByYear += enumBean.specialVacationDayByName("UNDER18");
        }
        
        if (emp.getHandicapped() == 1) {
            numberOfVacationByYear += enumBean.specialVacationDayByName("EMPLOYEEHANDICAPPED");
        }
        
        if (emp.getPositionId().getDangerous() == 1) {
            numberOfVacationByYear += enumBean.specialVacationDayByName("DANGEROUSCIRCUMSTANCE");
        }
        numberOfVacationByYear += childBean.vacationDaysAfterChildren(id);
        System.out.println(numberOfVacationByYear);
        try {
            numberOfVacationByYear = (int) Math.round(numberOfVacationByYear * percentOfTheYear(emp));
        } catch (ParseException ex) {
            Logger.getLogger(EmployeeBean.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        return numberOfVacationByYear;
    }
    
    private int ageOfEmployee(Date birthday) throws NumberFormatException {
        SimpleDateFormat dateYearFormat = new SimpleDateFormat("yyyy");
        return Integer.parseInt(dateYearFormat.format(new Date())) - Integer.parseInt(dateYearFormat.format(birthday));
    }
    
    private double percentOfTheYear(Employees emp) throws ParseException {
        DateFormat dateYearFormat = new SimpleDateFormat("yyyy");
        DateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        int year = Integer.parseInt(dateYearFormat.format(new Date()));
        
        int daysOfTheYear = 365;
        if ((year % 400 == 0) || (year % 4 == 0) && (year % 100 != 0)) {
            daysOfTheYear = 366;
        }
        
        long firstDayOfTheYear = dayFormat.parse(dateYearFormat.format(new Date()) + "-01-01").getTime();
        long lastDayOfTheYear = dayFormat.parse(dateYearFormat.format(new Date()) + "-12-31").getTime();
        
        long firstDayOfEmployee = emp.getStartofwork().getTime();
        
        int firstWorkDay = 0;
        if (firstDayOfEmployee < firstDayOfTheYear) {
            firstWorkDay = (int) (firstDayOfTheYear / 86400000);
        } else {
            firstWorkDay = (int) (firstDayOfEmployee / 86400000);
        }
        
        int lastWorkDay = 0;
        if (emp.getEndofwork() == null || lastWorkDay > lastDayOfTheYear) {
            lastWorkDay = (int) (lastDayOfTheYear / 86400000);
        } else {
            lastWorkDay = (int) (emp.getEndofwork().getTime() / 86400000);
        }
        
        int workDaysOfTheYear = lastWorkDay - firstWorkDay + 1;
        
        return (double) workDaysOfTheYear / daysOfTheYear;
    }
    
    @Override
    public List<EmployeesDTO> allEmployees() {
        List<EmployeesDTO> listOfEmployeesDTO = new ArrayList<>();
        TypedQuery<Employees> query = em.createNamedQuery("Employees.findAll", Employees.class);
        if (null != query) {
            for (Employees employee : query.getResultList()) {
                EmployeesDTO edto = employeeMapper.Entity2DTO(employee);
                listOfEmployeesDTO.add(edto);
            }
        }
        return listOfEmployeesDTO;
    }
    
    @Override
    public List<EmployeesDTO> allEmployeesByBossID(int bossID) {
        int departmentID = em.createQuery("SELECT d FROM Departments d WHERE d.departmentBossid=:bossID", Departments.class)
                .setParameter("bossID", bossID).getSingleResult().getDepartmentId();
        List<Employees> emps = em.createQuery("SELECT DISTINCT e FROM Employees e WHERE e.departmentId=:depID", Employees.class)
                .setParameter("depID", departmentID).getResultList();
        List<EmployeesDTO> edtos = new ArrayList<>();
        for (Employees emp : emps) {
            edtos.add(employeeMapper.Entity2DTO(emp));
        }
        return edtos;
    }
}
