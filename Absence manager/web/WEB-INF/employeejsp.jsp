<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EMPLOYEE</title>
        
        <jsp:include page="header.jsp"/>
        
        <script type="text/javascript" src="base.js"/>'></script>
     </head>
    <body>
       <h1>EMPLOYEE</h1>
     
       <br>
       <br>
        <form method="POST" action="/AbsenceEJB/EmployeeServlet"> 
           Firstname: <input class="form-control" type="text" name="firstname" />
           <br>
           Lastname: <input class="form-control" type="text" name="lastname" />
           <br>
           Birthday: <input class="form-control" type="date" name="birthday" value="2000-01-01" min="1918-01-01" max="2018-12-31" />
           <br>
           Handicapped?
           <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
              <input type="radio" name="handicapped" value="0" autocomplete="off"> NO
            </label>
            <label class="btn btn-secondary">
              <input type="radio" name="handicapped" value="1" autocomplete="off"> YES, HANDICAPPED
            </label>
            </div>
           <br>
           <br>
           Contract?
           <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-secondary">
              <input type="radio" name="contractstyle" value="0" autocomplete="off"> INDEFINITE
            </label>
            <label class="btn btn-secondary">
              <input type="radio" name="contractstyle" value="1" autocomplete="off"> DEFINITE
            </label>
           </div>
           <br>
           <br>
           Start of Work: <input class="form-control" type="date" name="startofwork" value="2018-01-01" min="2018-01-01" max="2018-12-31" />
           
           End of Work: <input class="form-control" type="date" name="endofwork" value="2018-01-01" min="2018-01-01" max="2099-12-31" />
           <br>
           E-mail: <input class="form-control" type="text" name="email" />
           <br>
           
           Position_ID:<select class="form-control" name="positionID">
                       <c:forEach var="text" items="${list}">
                           <option>	${text} </option>
                           </c:forEach>
                  </select
           
          <!-- Position_ID: <input class="form-control" type="text" name="positionID" />
           <br> -->
           <button class="btn btn-danger" onmouseover="greeting()">ADD EMPLOYEE</button>
        </form>
    </body>
</html>

