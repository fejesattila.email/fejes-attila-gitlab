<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Absence - Login</title>
            <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
            <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
            <link rel="stylesheet" href="login.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row-fluid" >                                         
                <div class="col-md-offset-4 col-md-4" id="box">
                <h2 align="center" color="#fff">Absence-Login</h2>                       
                <hr>
                <c:if test="${loginSessionBean.loggedOut}">
                    <form class="form-horizontal" method="post" action="/AbsenceEJB/login" id="contact_form">
                    <fieldset>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input name="username" placeholder="Username" class="form-control" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input name="password" placeholder="Password" class="form-control" type="password">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                        <button type="submit" class="btn btn-md btn-danger pull-right">Login </button>
                        </div>
                    </div>
                    </fieldset>
                    </form>
                <c:if test="${loginSessionBean.errorMessage!=null}">
                <div style="color:red; padding:1em 1em;">
                <p><c:out value="${loginSessionBean.errorMessage}"/></p>
                </div>
                </c:if>
                </c:if>
                </div> 
            </div>
    </body>
</html>



