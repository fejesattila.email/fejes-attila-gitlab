
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ALL</title>
        <jsp:include page="header.jsp"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" >Távollét-nyilvántartó</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#">Saját adatok <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Recepciós-riport</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Részleg-adatok</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Admin-funkciók</a>
                </li>
              </ul>
                <span class="navbar-text">
                <a class="nav-link" href="#">Beállítások</a>
              </span> 
              <span class="navbar-text">
                <a class="nav-link" href="#">Kilépés</a>
              </span>
            </div>
          </nav>
        <br>
        <form method="GET" action="/AbsenceEJB/PositionServlet"> 
        <button class="btn btn-outline-info">új pozi hozzáadása</button>
        </form>
        <br>
        <br>
        <form method="GET" action="/AbsenceEJB/EmployeeServlet"> 
        <button class="btn btn-outline-info">új dolgozó hozzáadása</button>
        </form>
        <br>
        <br>
        <form method="GET" action="/AbsenceEJB/ChildServlet"> 
        <button class="btn btn-outline-info">új gyerek hozzáadása</button>
        </form>
        <br>
        <br>
        <form method="GET" action="/AbsenceEJB/DepartmentServlet"> 
        <button class="btn btn-outline-info">új részleg hozzáadása</button>
        </form>
        <br>
    </body>
</html>
