<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RECEPTION</title>
        <jsp:include page="header.jsp"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" >TGN-ABSENCE</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#">Home Page<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/AbsenceEJB/rest/link/dailyabsenceriport">Reception-riport</a>
                </li>
              </ul>
                <span class="navbar-text">
                <a class="nav-link" href="#">Settings</a>
              </span> 
                  <span class="navbar-text">
                      
                      <label>${username}</label>
              </span> 
              <span class="navbar-text">
                <a class="nav-link" href="/AbsenceEJB/LogoutServlet">Logout</a>
              </span>
            </div>
          </nav>
              <br><br>
            <div class="row">
                <div class="col Default"></div>
                <div class="col Default">Number of Days</div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
                        <br>
            <div class="row">
                <div class="col bg-light">Annual entitlement: </div>
                <div class="col bg-light">${numofyearlyvacation}</div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
			<br>
            <div class="row">
                <div class="col bg-success">Available days: </div>
                <div class="col bg-success">${leftvacationdays}</div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
			<br>
            <div class="row">
                <div class="col bg-warning">Taken days: </div>
                <div class="col bg-warning">${takedoutvacationdays}</div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
			<br>
            <div class="row">
                <div class="col bg-danger">Sick leave: </div>
                <div class="col bg-danger">${sickdays}</div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
                        <br>
            <div class="row">
                <div class="col bg-info">Taken :</div>
                <div class="col bg-info">${vacpercent}</div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
        
        <br>
        
        <form method="GET" action="/AbsenceEJB/AbsenceServlet"> 
        <button class="btn btn-success btn-lg">ADD NEW ABSENCE-REQUEST</button>
        </form>
    </body>
</html>

