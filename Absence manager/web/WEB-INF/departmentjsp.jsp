<%@page import="java.util.HashMap"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DEPARTMENT</title>
        <jsp:include page="header.jsp"/>
    </head>
    <body>
        <div class="container">
            
            <form method="POST" action="/AbsenceEJB/DepartmentServlet">
                <div class="row">
                    <label for="departmentname">Department name:</label>
                    <input id="departmentname" class="form-control" type="text" name="departmentname">
                </div>
                
                <div class="row">
                    Department's boss:<select class="form-control" name="bossname">
                              <c:forEach var="text" items="${list}">
                           <option>	${text} </option>

                           </c:forEach>
                     </select>
                  </div>  
                 
                <div class="row">
                    <button class="btn btn-danger">ADD DEPARTMENT</button>
                </div>
            </form>
        </div>
        
        
        
    </body>
</html>
