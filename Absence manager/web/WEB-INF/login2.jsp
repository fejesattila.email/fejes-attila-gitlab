<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AbsenceWeb - Login</title>
        
        <jsp:include page="header.jsp"/>
    </head>
    <body>
        <c:if test="${loginSessionBean.loggedOut}">
        <form method="post" action="/AbsenceEJB/login">
            <input type="text" name="username" id="username" placeholder="Username"><br>
            <input type="password" name="password" id="password" placeholder="Password"><br>
            <input type="submit" value="Log in">
        </form>
        <c:if test="${loginSessionBean.errorMessage!=null}">
            <div style="color:red; padding:1em 1em;">
                <p><c:out value="${loginSessionBean.errorMessage}"/></p>
            </div>
        </c:if>
        </c:if>
        
    </body>
</html>