<%@page import="hu.tgn.enums.AbsenceStatus"%>
<%@page import="hu.tgn.enums.AbsenceType"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ABSENCE</title>
        
        <jsp:include page="header.jsp"/>
    </head>
    <body>
        <h1>ABSENCE REQUEST</h1>
        
        <br>
        <br>
        
        <form method="POST" action="/AbsenceEJB/AbsenceServlet"> 
            <div class="row">
                
                <div class="col">Absence Start Day: </div>
                <div class="col"><input class="form-control" type="date" name="absencestartdate" value="2018-01-01" min="2018-01-01" max="2018-12-31" />
                </div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                
            </div>
            <br>
            <div class="row">
                
                <div class="col">Absence End Day: </div>
                <div class="col"><input class="form-control" type="date" name="absenceenddate" value="2018-01-01" min="2018-01-01" max="2018-12-31" />
                </div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                
            </div>
            <br>
            <div class="row">
                <div class="col">Absence Type: </div>
                <div class="col">
                    <select class="form-control" name="absencetype">
                    <% for (AbsenceType absenceType : AbsenceType.values()) { %>
                          <option><%= absenceType %></option>
                    <%}%> 
                    </select> 
                </div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
            <br>
            <button class="btn btn-success">ADD ABSENCE REQUEST</button>
        </form>
        
        <br>
          <a href="/AbsenceEJB/Start" class="btn btn-outline-danger" role="button" aria-disabled="true">BACK</a>
    </body>
</html>
