<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="user-info">
    <a href="/logout">Kijelentkezés</a><a href="/profile">Profil</a><br>
    <p>Felhasználónév: <c:out value="${loginSessionBean.loggedUser.userName}"/></p>
    <p>Jogosultsági szint: <c:out value="${loginSessionBean.loggedUser.role}"/></p>
    <c:if test="${loginSessionBean.loggedUser.firstLogin==1}">
        <div style="border: 1px solid red; padding: 1em 1em;">
            <h6>Kötelező jelszóváltoztatás</h6>
            <p>Most jelentkeztél be először a rendszerbe, kérlek változtasd meg az alapértelmezett jelszót!</p>
            <form action="login?action=changepassword" method="post">
                <label for="password1">Jelszó</label>
                <input type="password" name="password1" id="password1">
                <br>
                <label for="password2">Jelszó újra</label>
                <input type="password" name="password2" id="password2">
                <br>
                <input type="submit" value="Küldés">
            </form>
            <c:if test="${loginSessionBean.errorMessage!=null}">
            <div style="color:red; padding:1em 1em;">
                <p><c:out value="${loginSessionBean.errorMessage}"/></p>
            </div>
            </c:if>
        </div>
    </c:if>
    <hr>
    <br>
</div>